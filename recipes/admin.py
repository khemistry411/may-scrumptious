from django.contrib import admin
from .models import Recipe, Recipestep, Ingredients
# or you can use from recipes.models import Recipe

# Register your models here.
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
        #"author",
)

@admin.register(Recipestep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "step_number",
        "instruction",
        "id",
    )

@admin.register(Ingredients)
class IngredientsAdmin(admin.ModelAdmin):
    list_display = (
        "amount",
        "food_item",
        "recipe",
    )