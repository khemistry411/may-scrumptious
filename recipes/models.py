from django.db import models
from django.conf import settings

# Create your models here.
class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField(max_length=3000)
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    #author = models.CharField(max_length=200)
    calories = models.IntegerField()
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title

class Recipestep(models.Model):
    step_number = models.SmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        Recipe,
        related_name = "steps",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["step_number"]

class Ingredients(models.Model):
    amount = models.CharField(max_length=150)
    food_item = models.CharField(max_length=150)
    recipe = models.ForeignKey(
        Recipe,
        related_name="ingredients",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["food_item"]
        verbose_name_plural ="ingredients"
