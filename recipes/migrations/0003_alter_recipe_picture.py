# Generated by Django 4.2.1 on 2023-05-24 21:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0002_alter_recipe_picture'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipe',
            name='picture',
            field=models.URLField(max_length=3000),
        ),
    ]
